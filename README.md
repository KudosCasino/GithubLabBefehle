Command line instructions


Git global setup

git config --global user.name "Mirwan Akaygün"
git config --global user.email "mirwan@outlook.de"

Create a new repository

git clone https://gitlab.com/KudosCasino/GithubLabBefehle.git
cd GithubLabBefehle
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/KudosCasino/GithubLabBefehle.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/KudosCasino/GithubLabBefehle.git
git push -u origin --all
git push -u origin --tags